
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(5, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
#GPIO.setup(5, GPIO.IN)

def add_rotation(channel):
	print "Moved"
#GPIO.add_event_detect(5, GPIO.BOTH,  callback=add_rotation)
#GPIO.add_event_callback(5, callback=add_rotation)

current_time = time.time()
rotations = 0

while current_time + 100 >  time.time():
	if GPIO.input(5) == True:
		rotations += 1
		print "adding one"
	if GPIO.wait_for_edge(5, GPIO.FALLING):
		print rotations

print 100.0/rotations *1.4
GPIO.cleanup()
