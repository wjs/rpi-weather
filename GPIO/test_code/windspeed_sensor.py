
# demo of "BOTH" bi-directional edge detection  
# script by Alex Eames http://RasPi.tv  
# http://raspi.tv/?p=6791  
  
import RPi.GPIO as GPIO  
from time import sleep     # this lets us have a time delay (see line 12)  
  
GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering  
GPIO.setup(5, GPIO.IN)    # set GPIO25 as input (button)  

rotations = 0  

# Define a threaded callback function to run in another thread when events are detected  
def my_callback(channel):  
    global rotations
    if GPIO.input(5):     # if port 25 == 1  
        print "Rising edge detected on 25"  
	rotations += 1
  
# when a changing edge is detected on port 25, regardless of whatever   
# else is happening in the program, the function my_callback will be run  
GPIO.add_event_detect(5, GPIO.BOTH, callback=my_callback)  
  
try:  
    sleep(60)         # wait 30 seconds  
    print rotations
    print 1.492 * (rotations /60.0)
    print "Time's up. Finished!"  
  
finally:                   # this block will run no matter how the try block exits  
    GPIO.cleanup()         # clean up after yourself  
