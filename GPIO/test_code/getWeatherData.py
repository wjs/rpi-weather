#!/usr/bin/python


import RPi.GPIO as GPIO
from time import sleep
import Adafruit_DHT
import time

# use the board numbering
GPIO.setmode(GPIO.BCM)


def count_rotations(channel):
	global rotations
	if GPIO.input(5):
		rotations += 1

# wind speed is on pin 5
GPIO.setup(5, GPIO.IN)
direction_pin = 6

GPIO.add_event_detect(5, GPIO.BOTH, callback=count_rotations)
rotations = 0

def get_direction():
		reading = 0
		GPIO.setup(direction_pin, GPIO.OUT)
		GPIO.output(direction_pin, GPIO.LOW)
		time.sleep(0.1)

		GPIO.setup(direction_pin, GPIO.IN)
		# This takes about 1 millisecond p/loop
		while (GPIO.input(direction_pin) == GPIO.LOW):
			reading += 1
		print "Direction: ", reading

def get_temp():
	humidity, temperature = Adafruit_DHT.read_retry(22, 13)
	print "Humidity: %s, Temperature: %s" % (humidity, temperature)

def get_speed():
	global rotations
	rotations = 0
	sleep(10)
	print "Speed", 1.492/2 * (rotations/10.0)


while 1:
	get_temp()
	get_speed()
	get_direction()
	time.sleep(1)

