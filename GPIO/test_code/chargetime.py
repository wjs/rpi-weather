import RPi.GPIO as GPIO, time

pin = 13
GPIO.setmode(GPIO.BCM)

def charge_time():
	cycles = 0
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin, GPIO.LOW)
	# debounce
	time.sleep(0.1)
	
	# set to read
	GPIO.setup(pin, GPIO.IN)
	# Loop until pin goes high at 1.4V
	# capacitor reads high on GPIO
	stime = time.time() 
	while (GPIO.input(pin) == GPIO.LOW):
		cycles += 1
	return cycles, 3.3/1.4*(time.time() - stime)

while True:
  print charge_time()

