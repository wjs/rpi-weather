#!/usr/bin/python

import json
import pprint
import urllib
from httplib2 import Http
from apiclient import discovery, errors
from oauth2client.client import SignedJwtAssertionCredentials
from apiclient.discovery import build

import RPi.GPIO as GPIO
from time import sleep
import Adafruit_DHT
import time
import datetime
import csv
import sys

datafile = open('data.csv', 'w')
writer = csv.writer(datafile)

with open("pydrive.json") as f:
  secret = eval(f.read())

credentials = SignedJwtAssertionCredentials(secret['client_email'], secret['private_key'],
    'https://spreadsheets.google.com/feeds')


# use the board numbering
GPIO.setmode(GPIO.BCM)

def count_rotations(channel):
	global rotations
	if GPIO.input(5):
		rotations += 1

# wind speed is on pin 5
GPIO.setup(5, GPIO.IN)
GPIO.add_event_detect(5, GPIO.BOTH, callback=count_rotations)
rotations = 0

# wind vane pin
direction_pin = 6

def count_drops(channel):
	global drops
	drops += 1

# rain gauge on pin 13
GPIO.setup(19, GPIO.IN)
GPIO.add_event_detect(19, GPIO.BOTH, callback=count_drops)
drops = 0

def get_direction():

		directions = {'North': [4059], 'NorthWest': [7950, 14600], 'West': [14300], 'SouthWest': [1700, 1900],
			          'South': [500], 'SouthEast': [200, 296, 100], 'East': [150, 500], 'NorthEast': [1030, 830]}
		charge_time = 0
		direction = ''
		# drain the capacitor
		GPIO.setup(direction_pin, GPIO.OUT)
		GPIO.output(direction_pin, GPIO.LOW)
		time.sleep(0.1)

		# set to read data and read until the circuit goes high
		# which means the capacitor is charged
		GPIO.setup(direction_pin, GPIO.IN)
		while (GPIO.input(direction_pin) == GPIO.LOW):
			charge_time += 1
		for k, v in directions.items():
			for t in v:
				if t - .1 * t < charge_time < t + .1 * t:
					direction = k
		return direction

def get_temp():
	humidity, temperature = Adafruit_DHT.read_retry(22, 13)
	return (round(9.0/5 * temperature + 32, 4), round(humidity, 4))

def get_speed():
	global rotations
	avg_speed = 1.492/2 * (rotations/60.0)
	rotations = 0.0
	return round(avg_speed, 4)

def count_drops():
	global drops
	drops += 1


http = Http()
credentials.authorize(http)
drive = discovery.build('drive', 'v2', http=http)
url = 'https://spreadsheets.google.com/feeds/list/1lWOkpymgG1XvTBKzzOryh6Qn3xUttlrThVdBy_fnupM/od6/private/full'
headers = {'Content-type': 'application/atom+xml'}

while 1:
	t, h =  get_temp()
 	sleep(60)	
	body = '<entry xmlns="http://www.w3.org/2005/Atom" xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended"><gsx:speed>%s</gsx:speed><gsx:direction>%s</gsx:direction><gsx:temperature>%s</gsx:temperature><gsx:humidity>%s</gsx:humidity><gsx:rainfall>%s</gsx:rainfall><gsx:datestamp>%s</gsx:datestamp></entry>' % (get_speed(), get_direction(), t, h, drops * .011, datetime.datetime.now())
	response, content = http.request(url, 'POST', headers=headers, body=body)
	drops = 0
