
This is the readme file for the setup up of the sparkfun weather meters:

		Product:
		https://www.sparkfun.com/products/8942

		Datasheet:
		https://www.sparkfun.com/datasheets/Sensors/Weather/Weather%20Sensor%20Assembly..pdf

Project parts list:

		1	Ceramic capacitor 	capacitance 1µF
		1	DHT22 Humidity and Temperature Sensor	
		1	T Cobbler Plus	
		1	560Ω Resistor	
		2	5.7kΩ Resistor	
		1	10kΩ Resistor	
		1	Raspberry Pi 2	
		1	SparkFun Weather Meters

![Fritzing Image](WeatherMeters_fritzing.png)

Anemometer

The meter has three sensors, wind speed, direction, and rainfall. The speed and direction 
meters share an rj-11 cable with the anemometer being on the two center leads and the vane
being on the outer two leads. The wind speed sensor is basically a switch that closes for
each rotation. Each rotation per second translates to a 1.492 mph wind. Accordingly, we will 
count the rotations of the anemometer and do a little math to output the speed. The data 
sheet reports:

		Anemometer

		The cup-type anemometer measures wind speed by closing a contact
		as a magnet moves past a switch. A wind speed of 1.492 MPH (2.4
		km/h) causes the switch to close once per second. 

That should mean that ((counter / samples)  * rfactor) = speed. But it looks like there are
two closings a second after doing a little timing. So we will divide by two to get the speed. 
In the program, I keep track of the rotations with an interrupt:

		def count_rotations(channel):
		        global rotations
		        if GPIO.input(5):
		                rotations += 1

		# wind speed is on pin 5
		GPIO.setup(5, GPIO.IN)
		GPIO.add_event_detect(5, GPIO.BOTH, callback=count_rotations)
		rotations = 0

In the main loop of the program, we just loop and sleep for a minute. So dividing the rotations
by 60 should give the average over the last minute. This won't be terribly accurate for instantaneous
wind speeds, but we can shorten the cycle to get better accuracy if desired.

Weather Vane.

The wind direction sensor has a series of 8 switches each attached to a different resistor.
The switches can be combined to produce 16 different resistances. The raspberry pi is digital
and as a result can not directly measure the voltage. But we can exploit a clever trick based
on how long it takes a capacitor in a circuit to charge. The longer it takes to charge the
bigger the resistor. As long as the times are consistent, we can utilize this to show which 
way the vane is pointing based on the time to charge the capacitor and the switch resistances. 
In our case we can use a 10K resistor and a 1 micro-farad capacitor. The full volatage on 
the circuit is 3.3V, but the pin goes high at 1.4V. The charge time to reach 63% of the Vmax 
is t = resistance * capacitance. According to theory the charge time for 2.1V is 1 second 
exactly. I get a charge time of .510494 seconds in practice:

		 pi counter, microseconds
		(120004, 	0.5104939937591553)
		(120083, 	0.5108890533447266)
		(120029, 	0.5105700492858887)

The charge time curve looks pretty linear and since this is only 70% of the 63% max mark we 
should multiply by 2/1.4 ~ 1.43 which give 0.7293857 second charge time -- not bad considering.

None of that really matters though as long as the numbers are predictable we can use this for
the weather meter. We just need to translate the resistances to counter numbers to use as a 
pointer for the vane. Below are the resistances for the meter from the data sheet matched with
counter numbers I get with the using the circuit.

		Direction    Resistance     Pi
		(Degrees)    (Ohms)         Counter
		0   N        33k            4059 (arrow pointing towards rain bucket)
		22.5         6.57k          7950 (rotating counter clockwise)
		45           8.2k           14600
		67.5         891            14600 
		90  West     1k             1957
		112.5        688            1957
		135          2.2k           400
		157.5        1.41k          500
		180 South    3.9k           500 (opposing )
		202.5        3.14k          296
		225          16k            296
		247.5        14.12k         100
		270 East     120k           150
		292.5        42.12k         1030
		315          64.9k          830
		337.5        21.88k         830

Since we really just need to sample this, no need for an interrupt. We can just sample this code once per 
loop to get the overall direction:

		def get_direction():
		 
		    directions = {'North': [4059], 'NorthWest': [7950, 14600], 'West': [14300], 'SouthWest': [1700, 1900],
		                  'South': [500], 'SouthEast': [200, 296, 100], 'East': [150, 500], 'NorthEast': [1030, 830]}
		    charge_time = 0
		    direction = ''

		    # drain the capacitor
		    GPIO.setup(direction_pin, GPIO.OUT)
		    GPIO.output(direction_pin, GPIO.LOW)
		    time.sleep(0.1)

		    # set to read data and read until the circuit goes high
		    # which means the capacitor is charged
		    GPIO.setup(direction_pin, GPIO.IN)
		    while (GPIO.input(direction_pin) == GPIO.LOW):
		            charge_time += 1
		    for k, v in directions.items():
		            for t in v:
		                    if t - .1 * t < charge_time < t + .1 * t:
		                            direction = k
		    return direction


Rain gauge.

The rain gauge follows from the anemometer really. We are just counting switch closings. Since I want to 
track total rain as well as "instantaneous" amounts. I will write out the number of closings/minute (which
translates to .011 inches of rain) each time a log entry is made. Then we can analyze the data however we
like. We just need to set up an interrupt tied to a callback function and reset the drops each cycle:

		# rain gauge on pin 13
		GPIO.setup(19, GPIO.IN)
		GPIO.add_event_detect(19, GPIO.BOTH, callback=count_drops)
		drops = 0
 
		def count_drops():
			global drops
			drops += 1

Temperature sensor

I purchased the SMAKN DHT22 AM2302 Digital Temperature And Humidity (http://www.amazon.com/gp/product/B00MIBRFTI). 
It was a little cheaper than the Adafruit one, but still the same thing and uses the same library 
(https://github.com/adafruit/Adafruit_Python_DHT). This was really simple with the library, just install it and
called the following to read the temperature and humidity.

	import Adafruit_DHT
	humidity, temperature = Adafruit_DHT.read_retry(22, 13)

The backend for the sensor is written in C since the communication protocol is in the microsecond 
range (http://www.adafruit.com/datasheets/DHT22.pdf) Since there is no need to track this with interrupts, 
the program just calls it when appropriate.

Google Connection

In order to connect to google and write to a spreadsheet there we will obviously need to create a
spreadsheet. Create a spreadsheet and highlight rows 2-1000 and delete them since we will want to 
start with a fresh sheet and append as we go. Now put the names of the measurements you want to record
in the first row. The names have to match exactly when adding an entry so N.B. My sheet has the 
following as the columns.

speed	direction	temperature	humidity	rainfall	datestamp															

That is the easy part. Now go to the google developer console (https://console.developers.google.com/project)
and create a new project. When the project finishes creating, select the APIs & Auth then APIs, then select
google drive and enable the drive API.

Next, in credentials "add credentials" and select service account. Choose JSON as the credentials type, and 
download the json file to a safe location. In order to append to the sheet, we need to get the listfeed URL. 
This little program should work:

		#!/usr/bin/python
		 
		import json
		from httplib2 import Http
		from oauth2client.client import SignedJwtAssertionCredentials
		  
		# open service account file generated at google developer console.
		with open(PATH_TO_DEVELOPER_JSON.json) as f:
		  secret = eval(f.read())
		 
		# generate a JWT
		credentials = SignedJwtAssertionCredentials(secret['client_email'], secret['private_key'],
		    'https://spreadsheets.google.com/feeds')
		 
		# authenticate with JWT to get authenticated session
		http = Http()
		credentials.authorize(http)
		 
		# get a sheets listing
		sheets = http.request("https://spreadsheets.google.com/feeds/spreadsheets/private/full?alt=json", "GET")
		 
		sheetsdict = {}
		for i in json.loads(sheets[1])['feed']['entry']:
		        sheetsdict[i['title']['$t']] = i['link'][0]['href']
		 
		for i in sheetsdict:
		        print i
		        print "\t", json.loads(http.request(sheetsdict[i]+'?alt=json')[1])['feed']['link'][1]['href']

1listfeed URL for each sheet in your google drive folder. After we have identified
the listfeed URL writing to the file is pretty easy. In the body tag below, we just need to make sure that 
the names of the columns match the headers.


		import urllib
		from httplib2 import Http
		from oauth2client.client import SignedJwtAssertionCredentials
		from apiclient.discovery import build
		 
		with open(PATH_TO_DEVELOPER_JSON.json) as f:
		  secret = eval(f.read())
		 
		credentials = SignedJwtAssertionCredentials(secret['client_email'], secret['private_key'],
		    'https://spreadsheets.google.com/feeds')
		 
		http = Http()
		credentials.authorize(http)
		drive = discovery.build('drive', 'v2', http=http)
		body = '<entry xmlns="http://www.w3.org/2005/Atom" xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended"><gsx:speed>%s</gsx:speed><gsx:direction>%s</gsx:direction><gsx:temperature>%s</gsx:temperature><gsx:humidity>%s</gsx:humidity><gsx:rainfall>%s</gsx:rainfall><gsx:datestamp>%s</gsx:datestamp></entry>' % (get_speed(), get_direction(), t, h, drops * .011, datetime.datetime.now())
		response, content = http.request(url, 'POST', headers=headers, body=body)
		drops = 0
		url = 'https://spreadsheets.google.com/feeds/list/LISTFEED-ID/od6/private/full'
		headers = {'Content-type': 'application/atom+xml'}
		response, content = http.request(url, 'POST', headers=headers, body=body)
		print response, content

That should append to the sheet every cycle. Now we need to move onto presentation.

Highcharts

Highcharts has a great demo 

http://www.highcharts.com/samples/view.php?path=highcharts/demo/combo-meteogram#http://www.yr.no/place/United_Kingdom/England/London/forecast_hour_by_hour.xml

and I thought it would be create to adapt our little station to this format. Not being a very proficient javascript 
coder, I opted for the path of least resistance which was formatting my data to fit into the spot of the yr.no
data. The data structure for tr.no was fairly detailed, but this works:

{
    "location": {
      "country": "United States", 
      "location": {
         "@attributes": {
            "latitude": "LATITUDE", 
            "altitude": "ALT", 
            "geobaseid": "GEOBASE", 
            "longitude": "LONGITUDE", 
            "geobase": "geonames"
         }
      }, 
      "type": "Populated place", 
      "name": "Shamblin House", 
      "timezone": {
         "@attributes": {
            "id": "America\\/New_York", 
            "utcoffsetMinutes": "-240"
         }
      }
   }, 
   "links": {
      "link": []
   }, 
   "forecast": {
      "tabular": {
         "time": []
      }
   }
}

inside the time list, we create readings like this:

               {
                    "comment": [
                        {}, 
                        {}
                    ], 
                    "temperature": {
                        "@attributes": {
                            "unit": "fahrenheit", 
                            "value": "70.34"
                        }
                    }, 
                    "precipitation": {
                        "@attributes": {
                            "value": "0"
                        }
                    }, 
                    "symbol": {
                        "@attributes": {
                            "var": "04", 
                            "numberEx": "4", 
                            "name": "Cloudy", 
                            "number": "4"
                        }
                    }, 
                    "humidity": {
                        "@attributes": {
                            "unit": "RH", 
                            "value": "89.8"
                        }
                    }, 
                    "pressure": {
                        "@attributes": {
                            "unit": "hPa", 
                            "value": "1019.6"
                        }
                    }, 
                    "windSpeed": {
                        "@attributes": {
                            "name": "Calm", 
                            "mph": "0.1368"
                        }
                    }, 
                    "@attributes": {
                        "to": "2015-09-27 19:30:48", 
                        "from": "2015-09-27 19:29:48"
                    }, 
                    "windDirection": {
                        "@attributes": {
                            "code": "E", 
                            "name": "East", 
                            "deg": 0
                        }
                    }
                }

Of course this all has to be pulled from the sheet at google and formatted, so we will need a script
for that. It turns out that we will need a script for highcharts that feeds the json data into the
javascript to get around cross-domain json data anyway so let's combine that into one php program.

Google Charts

As I was searching around for other options I discovered google charts. I had no idea they had a graphing API. 
It is pretty neat and since the data is already in google it should be pretty easy to setup. Really all you 
need to do is change the permissions on the sheet created above to public in google drive and edit the 
Google/weather.html file to reflect your sheet. Obviously there are lots of other things you could do with
google charts. For more take a look at the reference here:

https://developers.google.com/chart/?hl=en


Helpful resources:

	IP Anemometer
	http://zieren.de for more information.

	WiringPi:
	http://wiringpi.com/

	WeatherMeters:
	http://softwarefun.org/

	Basic Resistor Sensor Reading on Raspberry Pi
	https://learn.adafruit.com/basic-resistor-sensor-reading-on-raspberry-pi/overview
