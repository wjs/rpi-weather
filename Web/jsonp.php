<?php
/**
 * Set the callback variable to what jQuery sends over
 */
$callback = (string)$_GET['callback'];
if (!$callback) $callback = 'callback';
/**
 * The $filename parameter determines what file to load from local source
 */
$filename = $_GET['filename'];
if (preg_match('/^[a-zA-Z\-\.]+\.json$/', $filename)) {
	$json = file_get_contents($filename);
}
if (preg_match('/^[a-zA-Z\-\.]+\.csv$/', $filename)) {
	$csv = str_replace('"', '\"', file_get_contents($filename));
	$csv = preg_replace( "/[\r\n]+/", "\\n", $csv);
	$json = '"' . $csv . '"';
}
/**
 * The $url parameter loads data from external sources
 */
$json = file_get_contents("js.json.back");
//$json = exec("/usr/bin/python /u/wjs/WeatherMeters/Google/make_data.py");
// Send the output
header('Content-Type: text/javascript');
echo "$callback($json);";
?>
